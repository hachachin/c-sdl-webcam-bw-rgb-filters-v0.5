/*
    @author : BENTALEB CHADI
*/
#ifndef IMGC_H_INCLUDED
#define IMGC_H_INCLUDED
#include <stdbool.h>

#define FILTER_1 0
#define FILTER_2 1
#define FILTER_3 3

static bool HISTO_IS_NULL=true;
///******************************************STRUCTS
typedef struct pixel
{
    unsigned char RGB[3];
} Pixel;

typedef struct picture ///RGB image
{
    char name[50];
    int height,width;
    Pixel **image;
    int H[256];

} Picture;

typedef struct BWpicture///gray image
{
    char name[50];
    int height,width;
    unsigned char **image;
    int H[256];
    float Hn[256];

} BWPicture;

///********************************************FUNCTIONS

void allocateImageBW(BWPicture *img);///allocate unsigned char**image Black&white @param : Struct BWPicture
void allocateImage(Picture *img);///allocate Pixel ** image RGB image @param : width and height
Pixel ** allocateImageDim(int width, int height);///allocate Pixel ** image RGB image
unsigned char ** allocateImageDimBW(int width,int height);///allocate BW image @param : int width, int height
void drawRows(Picture *img,int size);///draw rows in a picture
void reverseImage(Picture *img); ///rotate image 90 degree
Picture fuse(Picture *pic1,Picture *pic2); ///fuse images
void zoomIO(Picture *pic,float degree); ///make pic resoution bigger if degree> 1, smaller if degree <1
int  posMin(BWPicture *img,int a,int b); /// get position of minimum value in a [a,b]
float Max(BWPicture *img);///get max value in Histogram of the img
int * Seuils(BWPicture *img);///get edges of histogram
void SimplifyHisto(BWPicture *img,int degree); /// make Histogram smooth
//BWPicture accumilateHistogram(BWPicture img);
int*  countPonts(BWPicture *img); ///get position of the peaks of img->Histogram (maximum locaux)
int*  countCurve(BWPicture *img); ///get position of the  lower points of img->Histogram (minimum locaux)
BWPicture drawHistogram(BWPicture *img,char * name);///make histogram of image (*img) to a BWPicture 256x300
BWPicture imprtToImageBW(char name[50],int width,int height); ///import b&w raw image
Picture imprtToImage(char name[50],int width,int height);///import RGB raw image
void exportToFileBW(BWPicture *img); ///export to raw image
void exportToFile(Picture img); ///export to raw RGB image
void reDrawBW(BWPicture *img);  ///Redraw image using segments of histogram, if histo gram is null,
                                ///it will fill it, but there is no Histogram smoothing, its discouraged
void HistoNorm(BWPicture *img); ///normaliser histogram and redraw image using it
void HistoCm(BWPicture *img);   ///fill histogram cumulé
void fillHisto(BWPicture *img); ///fill histogram
void stretchHisto(BWPicture *img);///amelioration de contrast avec etirment de l'histogram
void egaliser(BWPicture *img);///amelioration de contrast avec egalisation, needs Histogram cumulé
//void filter(BWPicture *img,char*N);
BWPicture textToImage(char*text);///turn text into image !letters only! add more raw images20x20 of letters in chars folder for more
void fillWithColor(BWPicture *img,int color); ///Fill image with color
void toWindowBW(BWPicture *img); ///show image in window, need SDL2 to work
void toWindowRGB(Picture *img); ///show image in window, need SDL2 to work
void filter(BWPicture *img,char * N,int FILTER_TYPE); ///using filter 5x5 in a image
void quantification(BWPicture *img,int bit); ///quantification 1 bit for black and white only ... @param : BWPicture *, int bit
void InversImage(BWPicture *img);///Invers colors of image
void contrast(BWPicture *img,int a,int b);///change contrast of image make it between [a,b] , for etirment use [0,255]
#endif // IMGC_H_INCLUDED
