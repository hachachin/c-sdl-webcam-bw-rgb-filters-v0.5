#include <stdio.h>
#include <stdlib.h>
#include "imgC.h"
#include "v4l2.h"
/*
    @author : BENTALEB CHADI

    *requirements:
        >Linux for Camera capture.
        >SDL2 to show images without imageJ

    *for compiling the file: add "-lSDL2" to compile with SDL2,

        gcc -Wall -g  -c "main.c" -o main.o
        gcc -Wall -g  -c "src.c" -o src.o
        gcc -Wall -g  -c "v4l2.c" -o v4l2.o
        g++  -o RGB main.o src.o v4l2.o  -lSDL2

    *execute :
        ./RGB
*/

///******************************************main
int main()
{
    ///Init image
    //Picture img=imprtToImage("1920x1080.raw",1920,1080);
    //BWPicture img=imprtToImageBW("collineraw385H289.raw",385,289);
    //BWPicture img=imprtToImageBW("lighthouse_8bits.raw",512,768);
    //lighthouse_8bits.raw
    ///Using CAM
    //BWPicture img=take_a_ShotBW(640,480,"/dev/video0",NOT_IN_SDL);// SHOW_IN_SDL NOT_IN_SDL
    //real_time_captureBW(640,480,"/dev/video0");
    //real_time_capture(640,480,"/dev//video0");
    ///Quantification @param BWPicture*, NOMBRE DE BITS
    //quantification(&img,0);
    ///Histogram
    //fillHisto(&img);
    //stretchHisto(&img);
    //HistoCm(&img);
    //HistoNorm(&img);
    //egaliser(&img);
    //fillHisto(&img);
    ///init histo
    //SimplifyHisto(&img,5);
    //BWPicture histo=drawHistogram(&img,"HISTOegaliser");
    //toWindowBW(&histo);
    //fillHisto(&img);
    //SimplifyHisto(&img,10);
    //reDrawBW(&img);
    //stretchHisto(&img);
    //fillHisto(&img);
    //BWPicture histo1=drawHistogram(img,"HISTOstretch1");
    //BWPicture redrawd=reDrawBW(img,15);
    ///Filters
    //contrast(&img,50,150);
    //InversImage(&img);
    //white(&img);
    //filterMoyenne(&img,"gauss",0);
    ///Text to image
    BWPicture img=textToImage("hello world");///use only letters
    ///Show in Window
    //toWindowRGB(&img);
    //toWindowBW(&histo);
    toWindowBW(&img);
    ///Write to file
    //exportToFileBW(&img);
    //exportToFileBW(&histo);

    printf("\nDONE!\n");
    return 0;
}
