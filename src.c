#include "imgC.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "SDL2/SDL.h"
/*@author : BENTALEB CHADI*/

///******************************************Allocate Image BW
unsigned char ** allocateImageDimBW(int width,int height)
{
    //printf("ALLOCATING BW %s : %dx%d ...\n",img.name,img.width,img.height);
    unsigned char **tmp=malloc(sizeof(unsigned char*)*height);
    int j=0,i=0;
    for(j=0; j<height; j++)
    {
        tmp[j]=malloc(sizeof(unsigned char)*width);

    }
    for(j=0; j<height; j++)
    {
        for(i=0; i<width; i++)
        {
            tmp[j][i]=0;
        }
    }
    //printf("ALLOCATION DONE\n");
    return tmp;

}
///******************************************Allocate Image BW
void allocateImageBW(BWPicture *img)
{
    //printf("ALLOCATING BW %s : %dx%d ...\n",img.name,img.width,img.height);
    img->image=malloc(sizeof(unsigned char*)*img->height);
    int j=0,i;
    for(j=0; j<img->height; j++)
    {
        img->image[j]=malloc(sizeof(unsigned char)*img->width);

    }
    for(i=0;i<img->height;i++)
        for(j=0;j<img->width;j++)
        img->image[i][j]=255;
    //printf("ALLOCATION DONE\n");


}
///******************************************Allocate Image pixels
void allocateImage(Picture *img)
{
    //printf("ALLOCATING %s : %dx%d ...\n",img.name,img.width,img.height);
    img->image=malloc(sizeof(Pixel*)*img->height);
    int j=0;
    for(j=0; j<img->height; j++)
    {
        img->image[j]=malloc(sizeof(Pixel)*img->width);

    }

    //printf("ALLOCATION DONE\n");
}
///******************************************Allocate Image pixels
Pixel ** allocateImageDim(int width, int height)
{
    printf("ALLOCATING image : %dx%d ...\n",width,height);
    Pixel **tmp=malloc(sizeof(Pixel*)*height);
    int j=0;
    for(j=0; j<height; j++)
    {
        tmp[j]=malloc(sizeof(Pixel)*width);

    }
    return tmp;
    //printf("ALLOCATION DONE\n");
}
///******************************************draw rows
void drawRows(Picture *img,int size)
{
    int i=0,j=0;
    for(j=0; j<img->height; j++)
    {
        for(i=0; i<img->width; i++)
        {
            if((i/size)%2)
            {
                img->image[i][j].RGB[0]=255;
                img->image[i][j].RGB[1]=0;
                img->image[i][j].RGB[2]=0;
            }
        }
    }
}
///******************************************90 degre transformation
void reverseImage(Picture *img)
{
    ///check if img is null
    if(img->image==NULL)
    {
        printf("null pointer : NULL IMAGE !!!");
        system("exit");
    }
    ///init clone tmp picture
    sprintf(img->name,"%s_reversed",img->name);
     Pixel ** tmp=allocateImageDim(img->height,img->width);
    ///reverse image
    int i=0,j=0;
    for(j=0; j<img->height; j++)
    {
        for(i=0; i<img->width; i++)
        {
            tmp[i][j].RGB[0]=img->image[j][i].RGB[0];
            tmp[i][j].RGB[1]=img->image[j][i].RGB[1];
            tmp[i][j].RGB[2]=img->image[j][i].RGB[2];
        }
    }
    free(img->image);
    img->image=tmp;
}
///******************************************FUSE
Picture fuse(Picture *pic1,Picture *pic2)
{
    ///init vars
    Picture img;
    img.height=pic1->height;
    img.width=pic1->width;
    ///changing name and allocating
    strcpy(img.name,strcat(pic1->name," Fused"));
    allocateImage(&img);
    ///fusing
    int i=0,j=0;
    for(i=0; i<img.height; i++)
    {
        for(j=0; j<img.width; j++)
        {
            img.image[i][j].RGB[0]=(pic2->image[i][j].RGB[0]+pic1->image[i][j].RGB[0])/2;
            img.image[i][j].RGB[1]=(pic2->image[i][j].RGB[1]+pic1->image[i][j].RGB[1])/2;
            img.image[i][j].RGB[2]=(pic2->image[i][j].RGB[2]+pic1->image[i][j].RGB[2])/2;

        }
    }
    return img;
}
///******************************************FUSE
void fuseBW(BWPicture *pic1,BWPicture *pic2)
{
    //TODO

    float degree=pic1->height>pic2->height?(float)((pic1->height*pic1->width)/(pic2->height*pic2->width))
                 :(float)((pic2->height*pic2->width)/(pic1->height*pic1->width));

    int i=0,j=0;
    for(i=0; i<pic1->height; i++)
    {
        for(j=0; j<pic2->width; j++)
        {
            /* img.image[i][j].RGB[0]=(pic2.image[i][j].RGB[0]+pic1.image[i][j].RGB[0])/2;
             img.image[i][j].RGB[1]=(pic2.image[i][j].RGB[1]+pic1.image[i][j].RGB[1])/2;
             img.image[i][j].RGB[2]=(pic2.image[i][j].RGB[2]+pic1.image[i][j].RGB[2])/2;
            */
        }
    }
}
///******************************************Zoom bw
void bwZoomIO(BWPicture *img,float degree)
{
    printf("START ZOOMING...\n");
    int i=0,j=0,x,y,height,width;

    height=(int)img->height*degree;
    width=(int)img->width*degree;
    img->height=height;
    img->width=width;

    unsigned char **tmp=allocateImageDimBW(width,height);

    for(i=0; i<height; i++)
    {
        for(j=0; j<width; j++)
        {
            x=(int)(i/degree);
            y=(int)(j/degree);
            tmp[i][j]=img->image[x][y];
        }
    }

    //memccpy(img->image,tmp,sizeof(unsigned char));
    free(tmp);
    printf("DONE ZOOMING\n");
}
///******************************************ZOOM
void zoomIO(Picture *pic,float degree)
{
    printf("START ZOOMING...\n");
    if(degree == 1)
    {
        printf("Done zooming, nothing to zoom ! \n");
    return;
    }
    int i=0,j=0,x,y;
    int height=(int)pic->height*degree;
    int width=(int)pic->width*degree;
    // printf("zoomed resolution : %d x %d",img.width,img.height);
    //strcpy(.name,pic->name);
    strcat(pic->name,"_zoomed ");
    //strcat(img.name,(char)degree);
    Pixel **tmp=allocateImageDim(width,height);

    for(i=0; i<height; i++)
    {
        for(j=0; j<width; j++)
        {
            x=(int)(i/degree);
            y=(int)(j/degree);
            tmp[i][j].RGB[0]=pic->image[x][y].RGB[0];
            tmp[i][j].RGB[1]=pic->image[x][y].RGB[1];
            tmp[i][j].RGB[2]=pic->image[x][y].RGB[2];
        }
    }
    pic->height=height; pic->width=width;
    free(pic->image);
    pic->image=tmp;
    printf("DONE ZOOMING\n");
}

///******************************************MIN
int  posMin(BWPicture *img,int a,int b)
{
    int i,pos=0;
    float min=Max(img);
    //printf("A=%d , B=%d max= %f \n",a,b,min);
    for(i=a; i<=b; i++)
    {
        //printf("%d \n",img->H[i]);
        if(img->H[i]<min)
        {
            min=img->H[i];
            pos=i;
        }
    }
    return pos;
}
///******************************************MAX
float Max(BWPicture *img)
{

    int max=0,i;
    for(i=0; i<256; i++)
    {
        if(max<img->H[i])
        {
            max=img->H[i];
        }
    }
    return max;
}
///******************************************SEUILS HISTO
int * Seuils(BWPicture *img)
{

    int i;
    int *tab=malloc(sizeof(int)*2);
    tab[0]=0;
    tab[1]=0;
    for (i=0; i<255; i++)
    {
        if(img->H[i]<img->H[i+1])
        {
            tab[0]=i;
            break;
        }
    }

    for (i=255; i>-1; i--)
    {
        if(img->H[i]>img->H[i-1])
        {
            tab[1]=i;
            break;
        }
    }
    return tab;
}
///******************************************Simplify  HISTO
void SimplifyHisto(BWPicture *img,int degree)
{
    int i,j=0;

    for(j=0; j<degree; j++)
        for(i=1; i<255; i++)
        {
            //if(img->H[i]<img->H[i-1] || img->H[i+1]>img->H[i])
            img->H[i]=(img->H[i-1]+img->H[i+1]+img->H[i])/3;
        }
    return;
}
///******************************************REDRAW IMAGE
void reDrawBW(BWPicture *img)
{
    if(HISTO_IS_NULL)
    {
        printf("Notice: empty histogram, filling it by default and smoothing it, degree 2... \n");
        fillHisto(img);
        SimplifyHisto(img,2);
    }

    ///---------------
    int *seuils=countCurve(img);
    int k,i,j,lenght=0;

    ///lenght of seuils tab
    while(seuils[lenght]!=NULL)
    {

        lenght++;
    }

    ///--------------

    for(k=1; k<lenght-1; k++)
    {
        printf("interval : %d \n",seuils[k]);
        for(i=0; i<img->height; i++)
        {
            for(j=0; j<img->width; j++)
            {

                if(img->image[i][j]<seuils[k] && img->image[i][j]>=seuils[k-1] )
                {
                    img->image[i][j]=seuils[k-1];
                }
            }
        }
    }

}
///******************************************GET POINTS FROM HISTO
int*  countCurve(BWPicture *img)
{
    int i=0,count=0;
    int *ponts=countPonts(img);
    ///count ponts
    while(ponts[i]!=NULL)
    {
        count++;
        i++;
    }
    ///count curves
    int *curves=malloc(sizeof(int)*(count-1));
    ponts[count-2]=255;
    for(i=0; i<(count-2); i++)
    {
         //printf("POS MIN(img,%d,%d)\n",ponts[i],ponts[i+1]);
        curves[i]=posMin(img,ponts[i],ponts[i+1]);

    }
    return (curves);
}
///******************************************STRETCH HISTO
void  stretchHisto(BWPicture *img)
{
    int i,*mx=Seuils(&img),j;
    double a,b;
    printf("min=%d,max=%d \n",mx[0],mx[1]);
    for(i=0; i<img->height; i++)
    {
        for(j=0; j<img->width; j++)
        {
            ///y=(max-min)/255*x+min
            img->image[i][j]=((255.0/(float)(mx[1]-mx[0]))*(float)(img->image[i][j]-mx[0]));
        }
    }
}

///******************************************GET POINTS FROM HISTO
int*  countPonts(BWPicture *img)
{
    int i,count=0;


    for(i=1; i<255; i++)
    {
        if((img->H[i-1]<img->H[i] && img->H[i]>img->H[i+1])
                /*|| (img->H[i-1]>=img->H[i] && img->H[i]<img->H[i+1])
                || (img->H[i-1]>img->H[i] && img->H[i]<=img->H[i+1])*/)
        {
            //printf("place %d/256\n",i);
            count++;
        }
    }
    printf("NBR maximum locaux : %d \n--------------------\n",count);

    int *tab=malloc(sizeof(int)*count);
    for(i=0; i<count; i++) tab[i]=0;
    count=0;
    for(i=1; i<255; i++)
    {
        if((img->H[i-1]<img->H[i] && img->H[i]>img->H[i+1])
                /*|| (img->H[i-1]>=img->H[i] && img->H[i]<img->H[i+1])
                || (img->H[i-1]>img->H[i] && img->H[i]<=img->H[i+1])*/)
        {
            tab[count]=i;
            count++;
        }
    }
    //printf("count = %d", count);
    return (tab);
}
///******************************************Fill HISTOGRAM
void fillHisto(BWPicture *img)
{
    int i,j;
    for(i=0; i<256; i++)
    {
        img->H[i]=0;
    }
    //
    for(i=0; i<img->height; i++)
    {
        for(j=0; j<img->width; j++)
        {

            img->H[img->image[i][j]]+=1;
        }
    }
     HISTO_IS_NULL=false;
}
///******************************************Draw HISTOGRAM

BWPicture drawHistogram(BWPicture *img,char * name)
{
    if(HISTO_IS_NULL)
    {
        printf("Notice: empty Histogram, filling it by default ...\n");
        fillHisto(img);
    }

    int *count,*curves,i,j;
    ///SimplifyHisto(&img,degree);
    float max=Max(img);

    ///SEUILS
    int *tab=Seuils(img);
    printf("--------------\nSEUILS: \nmin: %i , max: %i \n",tab[0],tab[1]);

    ///Count collins
    count=countPonts(img);
    //printf("--------------\n number of maximum locaux  : %d \n number of minimum locaux  : %d\n",strlen(count),strlen(curves));
    ///
    BWPicture histo;
    histo.width=256;
    histo.height=300;
    strcpy(histo.name,name);
    allocateImageBW(&histo);

    ///
    double scale=300/max;
    int tmp,cnt=0,color=0;
    printf("scale: %f , max value: %f \n",scale,max);

    for(i=0; i<256; i++)
    {
        tmp=(int)((float)img->H[i]*scale);
        //printf("tmp=%d \n",tmp);
        if(count[cnt]==i)
        {
            color=150;
            cnt++;
        }
        else
        {
            color=0;
        }
        //printf("H[%d]=%i\n",i,img->H[i]);
        for(j=299; j>=(300-tmp); j--)
        {
            histo.image[j][i]=color;
        }
    }
    return histo;
}
///******************************************Read file BW
BWPicture imprtToImageBW(char name[50],int width,int height)
{
    printf("READING BW image : %s...\n",name);
    ///init vars
    BWPicture img;
    img.width=width;
    img.height=height;
    strcpy(img.name,name);
    allocateImageBW(&img);
    ///open file
    FILE *f=fopen(name,"r");
    ///reading from file
    int i=0,j=0;
    if(f==NULL)
    {
        fprintf(stderr, "\t !! Failed to open the file [%s]!!",name);
        exit(EXIT_FAILURE);
    }
    for(i=0; i<img.height; i++)
    {
        for(j=0; j<img.width; j++)
        {
            fscanf(f,"%c",&img.image[i][j]);
        }
    }
    printf("READING DONE \n");
    fclose(f);
    return img;

}

///******************************************Read file
Picture imprtToImage(char name[50],int width,int height)
{
    printf("READING FILE %s...\n",name);
    ///init vars
    Picture img;
    img.width=width;
    img.height=height;
    strcpy(img.name,name);
    img.image=allocateImageDim(width,height);

    ///open file
    FILE *f=fopen(name,"r");
    if(f==NULL)
    {
        fprintf(stderr, "Failed to open the file [%s]",name);
        exit(EXIT_FAILURE);
    }
    ///reading from file
    int i=0,j=0;
    for(i=0; i<img.height; i++)
    {
        for(j=0; j<img.width; j++)
        {
            fscanf(f,"%c",&img.image[i][j].RGB[0]);
            fscanf(f,"%c",&img.image[i][j].RGB[1]);
            fscanf(f,"%c",&img.image[i][j].RGB[2]);
        }
    }

    fclose(f);
    printf("READING DONE \n");
    return img;
}
///******************************************export to BW image file
void exportToFileBW(BWPicture *img)
{
    printf("Exporting image...");
    //strcpy(img.name,strcat(strcat(strcat(img.name,(char*)img.width),"x"),(char*)img.height));
    //strcat(img.name,(char)&img.width);
    //strcat(img.name,(char)&img.height);
    FILE *f=fopen(img->name,"w");

    int i=0,j=0;
    for(i=0; i<img->height; i++)
    {
        for(j=0; j<img->width; j++)
        {
            fprintf(f,"%c",img->image[i][j]);
        }
    }
    fclose(f);
    int size=(sizeof(unsigned char**)+sizeof(unsigned char*)*img->height+img->height*img->width*sizeof(unsigned char));
    int sizeHDD=img->height*img->width*sizeof(unsigned char);
    printf("\n size RAM: %d o, size HDD: %d o",size,sizeHDD);
    printf("\n size RAM: %f ko, size HDD: %f Ko\n",(float)size/1024,(float)sizeHDD/1024);
}
///******************************************export to file
void exportToFile(Picture img)
{
    int i=0,j=0;
    FILE *f=fopen(img.name,"w");
    for(i=0; i<img.height; i++)
    {
        for(j=0; j<img.width; j++)
        {
            fprintf(f,"%c",img.image[i][j].RGB[0]);
            fprintf(f,"%c",img.image[i][j].RGB[1]);
            fprintf(f,"%c",img.image[i][j].RGB[2]);
        }
    }
    fclose(f);
}
///******************************************HistoN
void HistoNorm(BWPicture *img)
{
    int i,j;
    if(img->H[0]==NULL)
    {
        printf("notice: image->Histogram is null... \n");
        exit(EXIT_FAILURE);
    }
    for(i=0; i<img->height; i++)
        for(j=0;j<img->width;j++)
            img->image[i][j]=255*(int)((float)img->H[img->image[i][j]]/(float)(img->height*img->width));

}
///******************************************HistoC
void HistoCm(BWPicture *img)
{
    int i;
    //fillHisto(img);
    for(i=1; i<256; i++)
    {
        img->H[i]+=img->H[i-1];
    }
}
///******************************************Egaliser
void egaliser(BWPicture *img)
{
    int i,j;
    strcat(img->name," egalise");
    for(i=0; i<img->height; i++)
    {
        for(j=0; j<img->width; j++)
        {
            //printf("%d/%d=%f \n",img->H[(img->image[i][j])],(img->width*img->height),100*(4-1)*(float)img->H[(img->image[i][j])]/(float)(img->width*img->height));
            img->image[i][j]=(int)((255)*(float)img->H[(img->image[i][j])]/(float)(img->width*img->height));
        }
    }
}
///******************************************Filter
void filter(BWPicture *img,char * N,int FILTER_TYPE)
{
    float somme;
    float filter[5][5];

    switch(FILTER_TYPE)
    {
    case 0:
    {
        float filter5X5[5][5]=
        {
            {-1,-1,-1,-1,-1},
            {-1,-1,-1,-1,-1},
            {-1,-1,50,-1,-1},
            {-1,-1,-1,-1,-1},
            {-1,-1,-1,-1,-1}
        };
        memcpy(&filter, &filter5X5, sizeof filter);
        //free(filter5X5);
        somme=50;
        break;
    }
    case 1:
    {float filter5X5[5][5]=
        {
            {0,-1,-2,-1,0},
            {-1,0,2,0,-1},
            {-2,2,8,2,-2},
            {-1,0,2,0,-1},
            {0,-1,-2,-1,0}
        };
        memcpy(&filter, &filter5X5, sizeof filter);
        //free(filter5X5);
        somme=8;
        break;
    }
    case 2:
    {
        float filter5X5[5][5]=
        {
            {-10,-5,0,5,10},
            {-5,0,5,10,5},
            {0,5,10,5,0},
            {5,10,5,0,-5},
            {10,5,0,-5,-10}
        };
        memcpy(&filter, &filter5X5, sizeof filter);
        //free(filter5X5);
        somme=50;
        break;
    }
    default :
    {
        float filter5X5[5][5]=
        {
            {1,1,1,1,1},
            {1,1,1,1,1},
            {1,1,1,1,1},
            {1,1,1,1,1},
            {1,1,1,1,1}
        };
        memcpy(&filter, &filter5X5, sizeof filter);
        //free(filter5X5);
        somme=25;
        break;
    }
    }
///****************************************************
    int i,j,k,l,m,n,S;
    for(i=5; i<img->height-5; i++)
    {
        for(j=5; j<img->width-5; j++)
        {
            S=0;
            for(k=i-2, m=0; m<5, k<i+2; m++, k++)
            {
                for(l=j-2,n=0; l<j+2,n<5; n++,l++)
                {
                    S+=(img->image[k][l]*filter[m][n]);
                }
            }
            //printf("%d\n",S/25);
            //printf("%d\n",img->image[i][j]);
            img->image[i][j]=(int)(S/(somme));
        }
    }
    //free(filter);
    strcat(img->name,N);
}
///******************************************Writing on image
BWPicture textToImage(char*text)
{
    BWPicture img,CHAR;
    int length=strlen(text);
    img.height=20;
    img.width=20*length;

    strcpy(img.name,text);
    allocateImageBW(&img);
    fillWithColor(&img,255);
    int i,j,k,l=0;

    char tmp,file[50];

    for(i=0; i<strlen(text); i++)
    {
        tmp=text[i];
        ///filtring inputs
        if(tmp>=97 && tmp<=122)
        {
            tmp-=32;
        }
        if(tmp==' ')
        {
            tmp='_';
        }

        sprintf(file,"chars/%c.raw",tmp);/*
        strcpy(file,"chars/");
        */printf("%s",file);
        /*tmp[0]=text[i];
        */CHAR=imprtToImageBW(file,20,20);
        printf("--------\n");


        for(j=0; j<20; j++)
        {
            for(k=0,l=(20*i); k<20,l<(20+(20*i)); k++,l++)
            {
                //printf(" [%d][%d]=%d \n",j,k,CHAR.image[j%20][k]);
                img.image[j][l]=CHAR.image[j][k];
            }
        }
    }
    return img;
}
///**********************************************BW make it white
void fillWithColor(BWPicture *img,int color)
{
    int i,j;
    for(i=0; i<img->height; i++)
    {

        for(j=0; j<img->width; j++)
        {
            img->image[i][j]=color;
        }
    }
}
///************************************************Open in Window
void toWindowBW(BWPicture *img)
{
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    int i,j;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(img->width, img->height, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);


    for (i = 0; i < img->height; ++i)
    {
        for(j=0; j<img->width; j++)
        {
            SDL_SetRenderDrawColor(renderer, img->image[i][j], img->image[i][j], img->image[i][j], 255);
            SDL_RenderDrawPoint(renderer, j, i);
            //SDL_RenderClear(renderer);
        }
    }
    SDL_RenderPresent(renderer);
    while (1)
    {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
///*********************************************************************** open in window RGB
void toWindowRGB(Picture *img)
{
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    int i,j;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(img->width, img->height, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);


    for (i = 0; i < img->height; ++i)
    {
        for(j=0; j<img->width; j++)
        {
            SDL_SetRenderDrawColor(renderer, img->image[i][j].RGB[0], img->image[i][j].RGB[1], img->image[i][j].RGB[2], 255);
            SDL_RenderDrawPoint(renderer, j, i);
            //SDL_RenderClear(renderer);
        }
    }
    SDL_RenderPresent(renderer);
    while (1)
    {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
///*********************************************************************** QUANTIFICATION
void quantification(BWPicture *img,int bit)
{
    if(bit<=0)
    {
        printf("Fatal error: invalid bit entry for quantification [bit=%d] \n",bit);
        exit(EXIT_FAILURE);
    }

    int k,i,j,tmp=0;
    bit=pow(2,bit);

    int distance=(int)(256.0/bit);
    unsigned char *interval=malloc(sizeof(unsigned char)*(bit+1));
    interval[0]=0;
    interval[bit]=255;
    //printf("distance: %d, size: %d\n",distance,bit+1);
    for(i=1; i<bit; i++)
    {
        tmp+=distance;
        interval[i]=tmp;
        //printf("inter : %d \n",tmp);
    }


    for(k=1; k<=bit+1; k++)
    {
        for(i=0; i<img->height; i++)
        {
            for(j=0; j<img->width; j++)
            {
                if(img->image[i][j]<interval[k] && img->image[i][j]>=interval[k-1] )
                {
                    img->image[i][j]=interval[k-1];

                }
                if(img->image[i][j]>=interval[bit-1])
                {
                    img->image[i][j]=interval[bit];
                }
            }
        }
    }
    free(interval);
}
///*********************************************************************** Inverse image
void InversImage(BWPicture *img)
{
    int i,j;
    for(i=0; i<img->height; i++)
    {
        for(j=0; j<img->width; j++)
        {
            img->image[i][j]=255-img->image[i][j];
        }
    }

}
///*********************************************************************** Control contrast
void contrast(BWPicture *img,int a,int b)
{
    int *s=Seuils(img);
    int A=s[0],B=s[1];
    free(s);

    float P=(float)(b-a)/(float)(B-A);
    float L=(float)(a*B-A*b)/(float)(B-A);
    printf("A=%d, B=%d, P=%f,L=%f\n",A,B,P,L);


    int i,j;
    for(i=0; i<img->height; i++)
    {
        for(j=0; j<img->width; j++)
        {
            img->image[i][j]=(int)(P*img->image[i][j])+L;
        }
    }
}

