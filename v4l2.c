#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <asm/types.h>
#include <linux/videodev2.h>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <jpeglib.h>
#include "SDL2/SDL.h"

#include "v4l2.h"
/*@author : BENTALEB CHADI*/
/*  Found this code in GITHUB (use cam only), and changed to it to work for me,
    added some more functions

*/

void quit(const char * msg)
{
    fprintf(stderr, "[%s] %d: %s\n", msg, errno, strerror(errno));
    exit(EXIT_FAILURE);
}

int xioctl(int fd, int request, void* arg)
{
    for (int i = 0; i < 100; i++)
    {
        int r = ioctl(fd, request, arg);
        if (r != -1 || errno != EINTR) return r;
    }
    return -1;
}

camera_t* camera_open(const char * device, uint32_t width, uint32_t height)
{
    int fd = open(device, O_RDWR | O_NONBLOCK, 0);
    if (fd == -1) quit("open");
    camera_t* camera = malloc(sizeof (camera_t));
    camera->fd = fd;
    camera->width = width;
    camera->height = height;
    camera->buffer_count = 0;
    camera->buffers = NULL;
    camera->head.length = 0;
    camera->head.start = NULL;
    return camera;
}

///***************************************************************************** checking capabilities and setting formats
void camera_init(camera_t* camera)
{
    struct v4l2_capability cap;
    if (xioctl(camera->fd, VIDIOC_QUERYCAP, &cap) == -1) quit("VIDIOC_QUERYCAP");
    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) quit("no capture");
    if (!(cap.capabilities & V4L2_CAP_STREAMING)) quit("no streaming");

    struct v4l2_cropcap cropcap;
    memset(&cropcap, 0, sizeof cropcap);
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (xioctl(camera->fd, VIDIOC_CROPCAP, &cropcap) == 0)
    {
        struct v4l2_crop crop;
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect;
        if (xioctl(camera->fd, VIDIOC_S_CROP, &crop) == -1)
        {
            // cropping not supported
        }
    }

    struct v4l2_format format;
    memset(&format, 0, sizeof format);
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width = camera->width;
    format.fmt.pix.height = camera->height;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    format.fmt.pix.field = V4L2_FIELD_NONE;
    if (xioctl(camera->fd, VIDIOC_S_FMT, &format) == -1) quit("VIDIOC_S_FMT");

    struct v4l2_requestbuffers req;
    memset(&req, 0, sizeof req);
    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;
    if (xioctl(camera->fd, VIDIOC_REQBUFS, &req) == -1) quit("VIDIOC_REQBUFS");
    camera->buffer_count = req.count;
    camera->buffers = calloc(req.count, sizeof (buffer_t));

    size_t buf_max = 0;
    for (size_t i = 0; i < camera->buffer_count; i++)
    {
        struct v4l2_buffer buf;
        memset(&buf, 0, sizeof buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;
        if (xioctl(camera->fd, VIDIOC_QUERYBUF, &buf) == -1)
            quit("VIDIOC_QUERYBUF");
        if (buf.length > buf_max) buf_max = buf.length;
        camera->buffers[i].length = buf.length;
        camera->buffers[i].start =
            mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED,
                 camera->fd, buf.m.offset);
        if (camera->buffers[i].start == MAP_FAILED) quit("mmap");
    }
    camera->head.start = malloc(buf_max);
}

///***************************************************************************** tell the driver to start the cam
void camera_start(camera_t* camera)
{
    for (size_t i = 0; i < camera->buffer_count; i++)
    {
        struct v4l2_buffer buf;
        memset(&buf, 0, sizeof buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;
        if (xioctl(camera->fd, VIDIOC_QBUF, &buf) == -1) quit("VIDIOC_QBUF");
    }

    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (xioctl(camera->fd, VIDIOC_STREAMON, &type) == -1)
        quit("VIDIOC_STREAMON");
}
///***************************************************************************** tell the driver to stop the camera
void camera_stop(camera_t* camera)
{
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (xioctl(camera->fd, VIDIOC_STREAMOFF, &type) == -1)
        quit("VIDIOC_STREAMOFF");
}
///***************************************************************************** done using camera
void camera_finish(camera_t* camera)
{
    for (size_t i = 0; i < camera->buffer_count; i++)
    {
        munmap(camera->buffers[i].start, camera->buffers[i].length);
    }
    free(camera->buffers);
    camera->buffer_count = 0;
    camera->buffers = NULL;
    free(camera->head.start);
    camera->head.length = 0;
    camera->head.start = NULL;
}
///***************************************************************************** close camera
void camera_close(camera_t* camera)
{
    if (close(camera->fd) == -1) quit("close");
    free(camera);
}

///***************************************************************************** asking from driver to give us a capture
int camera_capture(camera_t* camera)
{
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    if (xioctl(camera->fd, VIDIOC_DQBUF, &buf) == -1) return FALSE;
    memcpy(camera->head.start, camera->buffers[buf.index].start, buf.bytesused);
    camera->head.length = buf.bytesused;
    if (xioctl(camera->fd, VIDIOC_QBUF, &buf) == -1) return FALSE;
    return TRUE;
}
///***************************************************************************** Take a frame
int camera_frame(camera_t* camera, struct timeval timeout)
{
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(camera->fd, &fds);
    int r = select(camera->fd + 1, &fds, 0, 0, &timeout);
    if (r == -1) quit("select");
    if (r == 0) return FALSE;
    return camera_capture(camera);
}
///***************************************************************************** IDK YET
int minmax(int min, int v, int max)
{
    return (v < min) ? min : (max < v) ? max : v;
}
///***************************************************************************** Convert to RGB
uint8_t* yuyv2rgb(uint8_t* yuyv, uint32_t width, uint32_t height)
{
    uint8_t* rgb = calloc(width * height * 3, sizeof (uint8_t));
    for (size_t i = 0; i < height; i++)
    {
        for (size_t j = 0; j < width; j += 2)
        {
            size_t index = i * width + j;
            int y0 = yuyv[index * 2 + 0] << 8;
            int u = yuyv[index * 2 + 1] - 128;
            int y1 = yuyv[index * 2 + 2] << 8;
            int v = yuyv[index * 2 + 3] - 128;
            rgb[index * 3 + 0] = minmax(0, (y0 + 359 * v) >> 8, 255);
            rgb[index * 3 + 1] = minmax(0, (y0 + 88 * v - 183 * u) >> 8, 255);
            rgb[index * 3 + 2] = minmax(0, (y0 + 454 * u) >> 8, 255);
            rgb[index * 3 + 3] = minmax(0, (y1 + 359 * v) >> 8, 255);
            rgb[index * 3 + 4] = minmax(0, (y1 + 88 * v - 183 * u) >> 8, 255);
            rgb[index * 3 + 5] = minmax(0, (y1 + 454 * u) >> 8, 255);
        }
    }
    return rgb;
}
///***************************************************************************** Take a snap
BWPicture take_a_ShotBW(int width,int height,char*dev_name,int SDL)
{
    camera_t* camera = camera_open(dev_name, width, height);
    camera_init(camera);
    camera_start(camera);
    printf("camera : %dx%d\n",camera->width, camera->height);

    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
    /* skip 5 frames for booting a cam */
    for (int i = 0; i < 5; i++)
    {
        camera_frame(camera, timeout);
    }
    BWPicture img;
    unsigned char* rgb;
    camera_frame(camera, timeout);
    rgb = yuyv2rgb(camera->head.start, camera->width, camera->height);
    img=to2DBW(width,height,rgb);

    printf("---------START---------\n");
    ///PUT FILTERS HERE------

    ///--------------------------
    if(SDL==0)
        {
            toWindowBW(&img);
        }


    free(rgb);
    camera_stop(camera);
    camera_finish(camera);
    camera_close(camera);
    return img;
}
///**************************************************************************convert to BW
BWPicture to2DBW(int width,int height,unsigned char *rgb)
{
    BWPicture img;
    strcpy(img.name,"FromCam.raw");
    img.height=height;
    img.width=width;
    allocateImageBW(&img);
    int i,j=-1,k;
    ///--------------------------------------------------------------------
    //printf("start to 2d bw\n");
    for(i=0,k=0; i<(height*width*3); k++,i=i+3)
    {
        if(i%(width*3)==0)
        {
            //printf("dkhol modulo \n");
            j++;
            k=0;
        }
        //printf("k=%d,j=%d , rgb[%d]=%d\n",k,j,i,rgb[i]);
        img.image[j][k]=(rgb[i]+rgb[i+1]+rgb[i+2])/3;
    }
    //printf("done to 2d bw\n");
    return img;

}
///**************************************************************************REAL TIME CAPTURE
void real_time_captureBW(int width,int height,char*dev_name)
{
    printf("-- START  CAPTURING BW\n");
    ///CAM THINGS
    camera_t* camera = camera_open(dev_name, width, height);
    camera_init(camera);
    camera_start(camera);
    printf("camera : %dx%d\n",camera->width, camera->height);

    ///SDL COMPONENTS
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(camera->width, camera->height, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    ///IDK why we using time but it works
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    int i,j;
    /* skip 5 frames for booting a cam */
    for (i = 0; i < 5; i++)
    {
        camera_frame(camera, timeout);
    }

    BWPicture img;
    unsigned char* rgb;


    while(1)
    {
        camera_frame(camera, timeout);
        rgb = yuyv2rgb(camera->head.start, camera->width, camera->height);
        img=to2DBW(width,height,rgb);///converting to buffer rgb to BW 2d table
        ///---PUT FILTERS HERE-------------


        ///-------------------------------
        for(i=0;i<img.height;i++)
            {
                for(j=0;j<img.width;j++)
                    {
                        SDL_SetRenderDrawColor(renderer, img.image[i][j],img.image[i][j],img.image[i][j], 255);
                        SDL_RenderDrawPoint(renderer, j,i);
                    }
            }
        SDL_RenderPresent(renderer);
        SDL_Delay(50);///delaying SDL so he wont kill my CPU

        ///for performance
        free(img.image);
        free(rgb);
    }
    while (1)
    {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    camera_stop(camera);
    camera_finish(camera);
    camera_close(camera);

}
///**************************************************************************convert to BW
Picture to2D(int width,int height,unsigned char *rgb)
{
    Picture img;
    strcpy(img.name,"FromCam.raw");
    img.height=height;
    img.width=width;
    allocateImage(&img);
    int i,j=-1,k;
    ///--------------------------------------------------------------------
    //printf("start to 2D \n");
    for(i=0,k=0; i<(height*width*3); k++,i=i+3)
    {
        if(i%(width*3)==0)
        {
            //printf("dkhol modulo \n");
            j++;
            k=0;
        }
        //printf("%d %d %d",rgb[i],rgb[i+1],rgb[i+2]);
        img.image[j][k].RGB[0]=rgb[i];
        img.image[j][k].RGB[1]=rgb[i+1];
        img.image[j][k].RGB[2]=rgb[i+1];
    }
    //printf("done to 2d bw\n");
    return img;

}
///**************************************************************************REAL TIME CAPTURE
void real_time_capture(int width,int height,char*dev_name)
{
    printf("-- START  CAPTURING\n");
    ///CAM THINGS
    camera_t* camera = camera_open(dev_name, width, height);
    camera_init(camera);
    camera_start(camera);
    printf("camera : %dx%d\n",camera->width, camera->height);

    ///SDL COMPONENTS
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(camera->width, camera->height, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    ///IDK why we using time but it works
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    int i,j;
    /* skip 5 frames for booting a cam */
    for (i = 0; i < 5; i++)
    {
        camera_frame(camera, timeout);
    }

    Picture img;
    unsigned char* rgb;


    while(1)
    {
        camera_frame(camera, timeout);
        rgb = yuyv2rgb(camera->head.start, camera->width, camera->height);
        img=to2D(width,height,rgb);///converting to buffer rgb to  2d array
        ///---PUT FILTERS HERE-------------

        ///-------------------------------
        for(i=0;i<img.height;i++)
            {
                for(j=0;j<img.width;j++)
                    {
                        SDL_SetRenderDrawColor(renderer, img.image[i][j].RGB[0],img.image[i][j].RGB[1]
                                               ,img.image[i][j].RGB[2], 255);
                        SDL_RenderDrawPoint(renderer, j,i);
                    }
            }
        SDL_RenderPresent(renderer);
        SDL_Delay(50);///delaying SDL so it wont kill my CPU

        ///for performance
        free(img.image);
        free(rgb);
    }
    while (1)
    {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    camera_stop(camera);
    camera_finish(camera);
    camera_close(camera);

}


