#ifndef V4L2_H_INCLUDED
#define V4L2_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "imgC.h"
#define SHOW_IN_SDL 0
#define NOT_IN_SDL 1
/*@author : BENTALEB CHADI*/
//static int SHOW_IN_SDL=0,NOT_IN_SDL=1;

typedef struct {
  uint8_t* start;
  size_t length;
} buffer_t;

typedef struct {
  int fd;
  uint32_t width;
  uint32_t height;
  size_t buffer_count;
  buffer_t* buffers;
  buffer_t head;
} camera_t;

void quit(const char * msg);
int xioctl(int fd, int request, void* arg);
int camera_frame(camera_t* camera, struct timeval timeout);
uint8_t* yuyv2rgb(uint8_t* yuyv, uint32_t width, uint32_t height);
int camera_capture(camera_t* camera);
void camera_close(camera_t* camera);
void camera_finish(camera_t* camera);
void camera_stop(camera_t* camera);
void camera_start(camera_t* camera);
void camera_init(camera_t* camera);
camera_t* camera_open(const char * device, uint32_t width, uint32_t height);
///my functions, notice: i dont own whats above it
BWPicture to2DBW(int width,int heigth,unsigned char *rgb); ///change buffer to 2D array
BWPicture take_a_ShotBW(int width,int height,char*dev_name,int SDL); ///take a picture B&W
void real_time_capture(int width,int height,char*dev_name); ///real time capture
void real_time_captureBW(int width,int height,char*dev_name);///real time capture RGB


#endif // V4L2_H_INCLUDED
